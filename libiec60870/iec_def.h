#ifndef IEC_DEF_H
#define IEC_DEF_H

// --------------------------------------------------------------------------------------------------------------------
// Process information in monitor direction
// 0: not defined
static const unsigned int M_SP_NA_1 = 1;    // single-point information
static const unsigned int M_SP_TA_1 = 2;    // single-point information with time tag
static const unsigned int M_DP_NA_1 = 3;    // double-point information 
static const unsigned int M_DP_TA_1 = 4;    // double-point information with time tag 
static const unsigned int M_ST_NA_1 = 5;    // step position information
static const unsigned int M_ST_TA_1 = 6;    // Step position information with time tag
static const unsigned int M_BO_NA_1 = 7;    // bitstring of 32 bits
static const unsigned int M_BO_TA_1 = 8;    // bitstring of 32 bits with time tag
static const unsigned int M_ME_NA_1 = 9;    // measured value, normalized value
static const unsigned int M_ME_TA_1 = 10;   // measured value, normalized value with time tag
static const unsigned int M_ME_NB_1 = 11;   // measured value, scaled value
static const unsigned int M_ME_TB_1 = 12;   // measured value, scaled value with time tag
static const unsigned int M_ME_NC_1 = 13;   // measured value, short floating point number
static const unsigned int M_ME_TC_1 = 14;   // measured value, short floating point number with time tag
static const unsigned int M_IT_NA_1 = 15;   // integrated totals
static const unsigned int M_IT_TA_1 = 16;   // integrated totals with time tag
static const unsigned int M_EP_TA_1 = 17;   // event of protection equipment with time tag
static const unsigned int M_EP_TB_1 = 18;   // packed start events of protection equipment with time tag
static const unsigned int M_EP_TC_1 = 19;   // packed output circuit information of protection equipment with time tag
static const unsigned int M_PS_NA_1 = 20;   // packed single-point information with status change detection
static const unsigned int M_ME_ND_1 = 21;   // measured value, normalized value without quality descriptor

// 22 .. 29 : reserved for further compatible definitions

// 30 .. 40 are defined on IEC 60870-5-101.
static const unsigned int M_SP_TB_1 = 30;   // single-point information with time tag CP56Time2a
static const unsigned int M_DP_TB_1 = 31;   // double-point information with time tag CP56Time2a
static const unsigned int M_ST_TB_1 = 32;   // step position information with time tag CP56Time2a
static const unsigned int M_BO_TB_1 = 33;   // bitstring of 32 bit with time tag CP56Time2a
static const unsigned int M_ME_TD_1 = 34;   // measured value, normalized value with time tag CP56Time2a
static const unsigned int M_ME_TE_1 = 35;   // measured value, scaled value with time tag CP56Time2a
static const unsigned int M_ME_TF_1 = 36;   // measured value, short floating point number 
static const unsigned int M_IT_TB_1 = 37;   // integrated totals with time tag CP56Time2a
static const unsigned int M_EP_TD_1 = 38;   // event of protection equipment with time tag CP56Time2a
static const unsigned int M_EP_TE_1 = 39;   // packed start events of protection equipment with time tag CP56Time2a
static const unsigned int M_EP_TF_1 = 40;   // packed output circuit information of protection equipment with time tag CP56Time2a

// 41 .. 44 : reserved for further compatible definitions

// --------------------------------------------------------------------------------------------------------------------
// Process information in control direction
static const unsigned int C_SC_NA_1 = 45;   // single command
static const unsigned int C_DC_NA_1 = 46;   // double command
static const unsigned int C_RC_NA_1 = 47;   // regulating step command
static const unsigned int C_SE_NA_1 = 48;   // set point command, normalized value
static const unsigned int C_SE_NB_1 = 49;   // set point command, scaled value
static const unsigned int C_SE_NC_1 = 50;   // set point command, short floating point number
static const unsigned int C_BO_NA_1 = 51;   // bitstring of 32 bits

// 52 .. 57 : reserved for further compatible definitions

static const unsigned int C_SC_TA_1 = 58;   // single command with time tag CP56Time2a  
static const unsigned int C_DC_TA_1 = 59;   // double command with time tag CP56Time2a
static const unsigned int C_RC_TA_1 = 60;   // regulating step command with time tag CP56Time2a
static const unsigned int C_SE_TA_1 = 61;   // set point command, normalized value with time tag CP56Time2a
static const unsigned int C_SE_TB_1 = 62;   // set point command, scaled value with time tag CP56Time2a
static const unsigned int C_SE_TC_1 = 63;   // set point command, short floating-point number with time tag CP56Time2a
static const unsigned int C_BO_TA_1 = 64;   // bitstring of 32 bits with time tag CP56Time2a

// 65 .. 69 : reserved for further compatible definitions

// --------------------------------------------------------------------------------------------------------------------
// System information in monitor direction
static const unsigned int M_EI_NA_1 = 70;   // end of initialization

// 71 .. 99 : reserved for further compatible definitions

// --------------------------------------------------------------------------------------------------------------------
// System information in control direction
static const unsigned int C_IC_NA_1 = 100;  // interrogation command   
static const unsigned int C_CI_NA_1 = 101;  // counter interrogation command
static const unsigned int C_RD_NA_1 = 102;  // read command 
static const unsigned int C_CS_NA_1 = 103;  // clock synchronization command (optional)
static const unsigned int C_RP_NA_1 = 105;  // reset process command
static const unsigned int C_TS_TA_1 = 107;  // test command with time tag CP56Time2a 

// 108 .. 109 : reserved for further compatible definitions

// --------------------------------------------------------------------------------------------------------------------
// Parameter in control direction
static const unsigned int P_ME_NA_1 = 110;  // parameter of measured value, normalized value
static const unsigned int P_ME_NB_1 = 111;  // parameter of measured value, scaled value
static const unsigned int P_ME_NC_1 = 112;  // parameter of measured value, short floating-point number
static const unsigned int P_AC_NA_1 = 113;  // parameter activation

// 114 .. 119 : reserved for further compatible definitions

// --------------------------------------------------------------------------------------------------------------------
// File transfer
static const unsigned int F_FR_NA_1 = 120;  // file ready
static const unsigned int F_SR_NA_1 = 121;  // section ready
static const unsigned int F_SC_NA_1 = 122;  // call directory, select file, call file, call section
static const unsigned int F_LS_NA_1 = 123;  // last section, last segment
static const unsigned int F_AF_NA_1 = 124;  // ack file, ack section 
static const unsigned int F_SG_NA_1 = 125;  // segment
static const unsigned int F_DR_TA_1 = 126;  // directory
static const unsigned int F_SC_NB_1 = 127;  // Query Log – Request archive file

#endif                                         
