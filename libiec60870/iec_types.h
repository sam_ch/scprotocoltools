#ifndef IEC_TYPES_H
#define IEC_TYPES_H

#ifdef __cplusplus
extern "C" {
#endif
    
#pragma pack(push)
#pragma pack(1)
    
    struct iec_stcd {
        union {
            unsigned short st;
            struct {
                unsigned char st1:   1;
                unsigned char st2:   1;
                unsigned char st3:   1;
                unsigned char st4:   1;
                unsigned char st5:   1;
                unsigned char st6:   1;
                unsigned char st7:   1;
                unsigned char st8:   1;
                unsigned char st9:   1;
                unsigned char st10:  1;
                unsigned char st11:  1;
                unsigned char st12:  1;
                unsigned char st13:  1;
                unsigned char st14:  1;
                unsigned char st15:  1;
                unsigned char st16:  1;
            };
        };
        union {
            unsigned short cd;
            struct {
                unsigned char cd1:   1;
                unsigned char cd2:   1;
                unsigned char cd3:   1;
                unsigned char cd4:   1;
                unsigned char cd5:   1;
                unsigned char cd6:   1;
                unsigned char cd7:   1;
                unsigned char cd8:   1;
                unsigned char cd9:   1;
                unsigned char cd10:  1;
                unsigned char cd11:  1;
                unsigned char cd12:  1;
                unsigned char cd13:  1;
                unsigned char cd14:  1;
                unsigned char cd15:  1;
                unsigned char cd16:  1;
            };
        };
    };
    
    struct iec_sep {
        unsigned char es:   2;  // Event State: 0 = ndeterminate state / 1 = OFF / 2 = ON / 3 = indeterminate state
        unsigned char res:  1;  //
        unsigned char ei:   1;  // 0 = elapsed time valid / 1 = elapsed time invalid
        unsigned char bl:   1;  // blocked / not blocked
        unsigned char sb:   1;  // substitued / not substitued
        unsigned char nt:   1;  // not topical / topical
        unsigned char iv:   1;  // valid /invalid
    };
    
    struct iec_qdp {
        unsigned char res:  3;  //
        unsigned char ei:   1;  // 0 = elapsed time valid / 1 = elapsed time invalid
        unsigned char bl:   1;  // blocked / not blocked
        unsigned char sb:   1;  // substitued / not substitued
        unsigned char nt:   1;  // not topical / topical
        unsigned char iv:   1;  // valid /invalid
    };
    
    struct iec_spe {
        unsigned char gs:   1;  // general start of operation, 0 = no general start of operation / 1 = general start of operation
        unsigned char sl1:  1;  // start of operation phase L1,  0 = no start of operation L1 / 1 = start of operation L1
        unsigned char sl2:  1;  // start of operation phase L2,  0 = no start of operation L2 / 1 = start of operation L2
        unsigned char sl3:  1;  // start of operation phase L3,  0 = no start of operation L3 / 1 = start of operation L3
        unsigned char sie:  1;  // start of operation IE (earth current), 0 = no start of operation / 1 = start of operation
        unsigned char srd:  1;  // start of operation in reverse direction, 0 = no start of operation in reverse direction / start of operation in reverse direction
        unsigned char res:  2;  //
    };
    
    struct iec_oci {
        unsigned char gc:   1;  // general command to output circuit, 0 = no general command to output circuit / 1 = general command to output circuit
        unsigned char cl1:  1;  // command to output circuit phase L1,  0 = no command to output circuit phase L1 / 1 = command to output phase circuit L1
        unsigned char cl2:  1;  // command to output circuit phase L2,  0 = no command to output circuit phase L2 / 1 = command to output phase circuit L2
        unsigned char cl3:  1;  // command to output circuit phase L3,  0 = no command to output circuit phase L3 / 1 = command to output phase circuit L3
        unsigned char res:  4;  //
    };
    
    struct cp56time2a {
        unsigned short msec;
        unsigned char min:      6;
        unsigned char res1:     1;
        unsigned char iv:       1;
        unsigned char hour:     5;
        unsigned char res2:     2;
        unsigned char su:       1;
        unsigned char mday:     5;
        unsigned char wday:     3;
        unsigned char month:    4;
        unsigned char res3:     4;
        unsigned char year:     7;
        unsigned char res4:     1;
    };
    
    struct cp24time2a {
        unsigned short msec;
        unsigned char min:  6;
        unsigned char res:  1;
        unsigned char iv:   1;
    };
    
    struct cp16time2a {
        unsigned short msec;
    };
    
    struct iec_type_1 {
        unsigned char sp:   1;  // single point information
        unsigned char res:  3;  //
        unsigned char bl:   1;  // blocked / not blocked
        unsigned char sb:   1;  // substitued / not substitued
        unsigned char nt:   1;  // not topical / topical
        unsigned char iv:   1;  // valid /invalid
    };
    
    struct iec_type_3 {
        unsigned char dp:   2;  // double point information
        unsigned char res:  2;  //
        unsigned char bl:   1;  // blocked / not blocked
        unsigned char sb:   1;  // substitued / not substitued
        unsigned char nt:   1;  // not topical / topical
        unsigned char iv:   1;  // valid /invalid
    };
    
    struct iec_type_5 {
        unsigned char mv:   7;  //
        unsigned char t:    1;  // transient flag
        unsigned char ov:   1;  // overflow / no overflow
        unsigned char res:  3;  //
        unsigned char bl:   1;  // blocked / not blocked
        unsigned char sb:   1;  // substitued / not substitued
        unsigned char nt:   1;  // not topical / topical
        unsigned char iv:   1;  // valid /invalid
    };
    
    struct iec_type_7 {
        struct iec_stcd stcd;
        unsigned char ov:   1;  // overflow / no overflow
        unsigned char res:  3;  //
        unsigned char bl:   1;  // blocked / not blocked
        unsigned char sb:   1;  // substitued / not substitued
        unsigned char nt:   1;  // not topical / topical
        unsigned char iv:   1;  // valid /invalid
    };
    
    struct iec_type_9 {
        short mv;               // normalized value
        unsigned char ov:   1;  // overflow / no overflow
        unsigned char res:  3;  //
        unsigned char bl:   1;  // blocked / not blocked
        unsigned char sb:   1;  // substitued / not substitued
        unsigned char nt:   1;  // not topical / topical
        unsigned char iv:   1;  // valid /invalid
    };
    
    struct iec_type_11 {
        short mv;               // scaled value
        unsigned char ov:   1;  // overflow / no overflow
        unsigned char res:  3;  //
        unsigned char bl:   1;  // blocked / not blocked
        unsigned char sb:   1;  // substitued / not substitued
        unsigned char nt:   1;  // not topical / topical
        unsigned char iv:   1;  // valid /invalid
    };
    
    struct iec_type_13 {
        float mv;               // float value
        unsigned char ov:   1;  // overflow / no overflow
        unsigned char res:  3;  //
        unsigned char bl:   1;  // blocked / not blocked
        unsigned char sb:   1;  // substitued / not substitued
        unsigned char nt:   1;  // not topical / topical
        unsigned char iv:   1;  // valid /invalid
    };
    
    struct iec_type_15 {
        unsigned int bcr;       // Binary counter reading, defined 7.2.6.9
        unsigned char sq:   5;  // overflow / no overflow
        unsigned char cy:   1;  // carry: 0 = no counter overflow occurred / 1 = counter overflow occurred
        unsigned char ca:   1;  // 0 = counter was not adjusted / 1 = counter was adjusted
        unsigned char iv:   1;  // valid /invalid
    };
    
    struct iec_type_30 {
        unsigned char sp:   1;  // single point information
        unsigned char res:  3;  //
        unsigned char bl:   1;  // blocked / not blocked
        unsigned char sb:   1;  // substitued / not substitued
        unsigned char nt:   1;  // not topical / topical
        unsigned char iv:   1;  // valid /invalid
        cp56time2a time;
    };
    
    struct iec_type_31 {
        unsigned char dp:   2;  // double point information
        unsigned char res:  2;  //
        unsigned char bl:   1;  // blocked / not blocked
        unsigned char sb:   1;  // substitued / not substitued
        unsigned char nt:   1;  // not topical / topical
        unsigned char iv:   1;  // valid /invalid
        cp56time2a time;
    };
    
    struct iec_type_32 {
        unsigned char mv:   7;  //
        unsigned char t:    1;  // transient flag
        unsigned char ov:   1;  // overflow / no overflow
        unsigned char res:  3;  //
        unsigned char bl:   1;  // blocked / not blocked
        unsigned char sb:   1;  // substitued / not substitued
        unsigned char nt:   1;  // not topical / topical
        unsigned char iv:   1;  // valid /invalid
        cp56time2a time;
    };
    
    struct iec_type_33 {
        union {
            struct iec_stcd stcd;
            unsigned int bsi;
        };
        unsigned char ov:   1;  // overflow / no overflow
        unsigned char res:  3;  //
        unsigned char bl:   1;  // blocked / not blocked
        unsigned char sb:   1;  // substitued / not substitued
        unsigned char nt:   1;  // not topical / topical
        unsigned char iv:   1;  // valid /invalid
    };
    
    struct iec_type_34 {
        short mv;
        unsigned char ov:   1;  // overflow / no overflow
        unsigned char res:  3;  //
        unsigned char bl:   1;  // blocked / not blocked
        unsigned char sb:   1;  // substitued / not substitued
        unsigned char nt:   1;  // not topical / topical
        unsigned char iv:   1;  // valid /invalid
        cp56time2a time;
    };
    
    struct iec_type_35 {
        short mv;
        unsigned char ov:   1;  // overflow / no overflow
        unsigned char res:  3;  //
        unsigned char bl:   1;  // blocked / not blocked
        unsigned char sb:   1;  // substitued / not substitued
        unsigned char nt:   1;  // not topical / topical
        unsigned char iv:   1;  // valid /invalid
        cp56time2a time;
    };
    
    struct iec_type_36 {
        float mv;
        unsigned char ov:   1;  // overflow / no overflow
        unsigned char res:  3;  //
        unsigned char bl:   1;  // blocked / not blocked
        unsigned char sb:   1;  // substitued / not substitued
        unsigned char nt:   1;  // not topical / topical
        unsigned char iv:   1;  // valid /invalid
        cp56time2a time;
    };
    
    struct iec_type_37 {
        unsigned int bcr;       // Binary counter reading, defined 7.2.6.9
        unsigned char sq:   5;  // overflow / no overflow
        unsigned char cy:   1;  // carry: 0 = no counter overflow occurred / 1 = counter overflow occurred
        unsigned char ca:   1;  // 0 = counter was not adjusted / 1 = counter was adjusted
        unsigned char iv:   1;  // valid /invalid
        cp56time2a time;
    };
    
    struct iec_type_45 {
        unsigned char scs:  1;  // Single command state, 0 = OFF / 1 = ON
        unsigned char res:  1;  // must be 0
        unsigned char qu:   5;  // define in 7.2.6.26
        unsigned char se:   1;  // 1 = selected / execute = 0
    };
    
    struct iec_type_46 {
        unsigned char dcs:  2;  // Double command state, 0,3 = not permitted / 1 = OFF / 2 = ON
        unsigned char qu:   5;  // define in 7.2.6.26
        unsigned char se:   1;  // 1 = selected / execute = 0
    };
    
    struct iec_type_47 {
        unsigned char rcs:  2;  // Double command state, 0,3 = not permitted / 1 = next step LOWER / 2 = next step HIGHER
        unsigned char qu:   5;  // define in 7.2.6.26
        unsigned char se:   1;  // 1 = selected / execute = 0
    };
    
    struct iec_type_48 {
        short nav;              // Normalized value, defined in 7.2.6.6
        unsigned char ql:   7;  // Qualifier of set-point command, defined in 7.2.6.39
        unsigned char se:   1;  // 1 = selected / execute = 0
    };
    
    struct iec_type_49 {
        short sva;              // Scaled value, defined in 7.2.6.7
        unsigned char ql:   7;  // Qualifier of set-point command, defined in 7.2.6.39
        unsigned char se:   1;  // 1 = selected / execute = 0
    };
    
    struct iec_type_50 {
        float r32;              // float
        unsigned char ql:   7;  // Qualifier of set-point command, defined in 7.2.6.39
        unsigned char se:   1;  // 1 = selected / execute = 0
    };
    
    struct iec_type_58 {
        unsigned char scs:  1;  // Single command state, 0 = OFF / 1 = ON
        unsigned char res:  1;  //
        unsigned char qu:   5;  // define in 7.2.6.26
        unsigned char se:   1;  // 1 = selected / execute = 0
        cp56time2a time;
    };
    
    struct iec_type_59 {
        unsigned char dcs:  2;  // Double command state, 0,3 = not permitted / 1 = OFF / 2 = ON
        unsigned char qu:   5;  // define in 7.2.6.26
        unsigned char se:   1;  // 1 = selected / execute = 0
        cp56time2a time;
    };
    
    struct iec_type_60 {
        unsigned char rcs:  2;  // // Double command state, 0,3 = not permitted / 1 = next step LOWER / 2 = next step HIGHER
        unsigned char qu:   5;  // define in 7.2.6.26
        unsigned char se:   1;  // 1 = selected / execute = 0
        cp56time2a time;
    };
    
    struct iec_type_61 {
        short nav;              // Normalized value, defined in 7.2.6.6
        unsigned char ql:   7;  // Qualifier of set-point command, defined in 7.2.6.39
        unsigned char se:   1;  // 1 = selected / execute = 0
        cp56time2a time;
    };
    
    struct iec_type_62 {  
        short sva;              // Scaled value, defined in 7.2.6.7
        unsigned char ql:   7;  // Qualifier of set-point command, defined in 7.2.6.39
        unsigned char se:   1;  // 1 = selected / execute = 0
        cp56time2a time;
    };
    
    struct iec_type_63 {  
        float sr32;              // Scaled value, defined in 7.2.6.7
        unsigned char ql:   7;  // Qualifier of set-point command, defined in 7.2.6.39
        unsigned char se:   1;  // 1 = selected / execute = 0
        cp56time2a time;
    };
    
    struct iec_type_64 {  
        struct iec_stcd stcd;   
        cp56time2a time;
    };
    
    struct iec_type_100 {  
        unsigned char qoi;  // Qualifier of interrogation, defined in 7.2.6.22
    };
    
    struct iec_type_101 {  
        unsigned char rqt:      6;  // Request
        unsigned char freeze:   2;  // freeze
    };
    
    struct iec_type_104 {
        unsigned short fbp;  // Fixed test pattern, defined in 7.2.6.14
    };
    
    struct iec_type_105 {
        unsigned char qrp;  // Qualifier of reset process command, defined in 7.2.6.27
    };
    
    struct iec_type_107 {
        unsigned short tsc;  // Compteur de séquence de test, 16 bits
        cp56time2a time;
    };
    
    struct iec_type_110 {
        unsigned short nva;     // Normalized value, defined in 7.2.6.6
        unsigned char kpa:  6;  // kind of parameter
        unsigned char lpc:  1;  // local parameter change, 0 = no change / 1 = change
        unsigned char pop:  1;  // parameter operation, 0 = in operation / 1 = not in operation
    };
    
    struct iec_type_111 {
        unsigned short sva;     // Scaled value, defined in 7.2.6.7
        unsigned char kpa:  6;  // kind of parameter
        unsigned char lpc:  1;  // local parameter change, 0 = no change / 1 = change
        unsigned char pop:  1;  // parameter operation, 0 = in operation / 1 = not in operation
    };
    
    struct iec_type_112 {
        float r32;              // Short floating point number, defined in 7.2.6.8
        unsigned char kpa:  6;  // kind of parameter
        unsigned char lpc:  1;  // local parameter change, 0 = no change / 1 = change
        unsigned char pop:  1;  // parameter operation, 0 = in operation / 1 = not in operation
    };
    
    struct iec_type_113 {
        unsigned char qpa;  //  Qualifier of parameter activation, defined in 7.2.6.25
    };
    
    struct iec_type_120 {
        unsigned short nof;     // Name of file
        unsigned int lof:   24; // Length of file
        unsigned int frq_ui7:   7;  // File ready qualifier, defined in 7.2.6.28
        unsigned int frq_bs:    1;  // 0 = positive confirm of select, request, deactivate or delete / 1 = negative confirm of select, request, deactivate or delete
    };
    
    struct iec_type_121 {
        unsigned short nof;         // Name of file
        unsigned char nos; // Length of file
        unsigned int lof:       24; // Length of section
        unsigned int srq_ui7:   7;  // Section ready qualifie, defined in 7.2.6.29
        unsigned int srq_bs:    1;  // 0 = section ready to load / 1 = section not ready to load
    };
    
    struct iec_type_122 {
        unsigned short nof;             // Name of file
        unsigned char nos;              // Name of section
        unsigned int lof:           24; // Length of section
        unsigned int scq_ui4_low:   4;  // Select and call qualifier, defined in 7.2.6.30
        unsigned int scq_ui4_high:  4;  // Select and call qualifier, defined in 7.2.6.30
    };
    
    struct iec_type_123 {
        unsigned short nof; // Name of file
        unsigned char nos;  // Name of section
        unsigned char lsq;  // Last section or segment qualifier, defined in 7.2.6.31
        unsigned char chs;  // Checksum, defined in 7.2.6.37
    };
    
    struct iec_type_124 {
        unsigned short nof; // Name of file
        unsigned char nos;  // Name of section
        unsigned char afq_ui4_low:   4;  // Acknowledge file or section qualifier, defined in 7.2.6.32
        unsigned char afq_ui4_high:  4;  // Acknowledge file or section qualifier, defined in 7.2.6.32
    };
    
    struct iec_type_125 {
        unsigned short nof; // Name of file
        unsigned char nos;  // Length of file
        unsigned char los;  // Length of segment, defined in 7.2.6.36
    };
    
    struct iec_type_126 {
        unsigned short nof; // Name of file or subdirectory. Defined in 7.2.6.33
        unsigned int lof:       24;  // Length of file
        unsigned int status:    5;  // Status of file, defined in 7.2.6.38
        unsigned int sof_lfd:   1;  // 0 = additional file of the directory follows / 1 = last file of the directory
        unsigned int sof_for:   1;  // 0 = name defines file / 1 = name defines subdirectory
        unsigned int sof_fa:    1;  // 0 = file waits for transfer / 1 = transfer of this file is activ
        cp56time2a time;
    };
    
    struct iec_101_unit_id {
        unsigned char type;     // Type identification
        unsigned char num:      7;  // number of INFORMATION OBJECTs or ELEMENTs
        unsigned char sq:       1;  // Single/sequence
        unsigned char cause:    6;  // CAUSE OF TRANSMISSION
        unsigned char pn:       1;  // 0 = positive confirm / 1 = negative confirm
        unsigned char t:        1;  // 0 = no test / 1 = test
        unsigned short ca;          // common address of ASDU
    };
    
    struct iec_104_unit_id {
        unsigned char type;     // Type identification
        unsigned char num:      7;  // number of INFORMATION OBJECTs or ELEMENTs
        unsigned char sq:       1;  // Single/sequence
        unsigned char cause:    6;  // CAUSE OF TRANSMISSION
        unsigned char pn:       1;  // 0 = positive confirm / 1 = negative confirm
        unsigned char t:        1;  // 0 = no test / 1 = test
        unsigned char originator;   // Originator address
        unsigned short ca;          // common address of ASDU
    };
    
    struct iec_apci {
        unsigned char start;
        unsigned char length;
        unsigned short ns;
        unsigned short nr;
    };
    
    struct iec_type {
        union {
            struct iec_type_1 type_1;
            struct iec_type_3 type_3;
            struct iec_type_7 type_7;
            struct iec_type_9 type_9;
            struct iec_type_11 type_11;
            struct iec_type_13 type_13;
            struct iec_type_15 type_15;
            struct iec_type_30 type_30;
            struct iec_type_31 type_31;
            struct iec_type_32 type_32;
            struct iec_type_33 type_33;
            struct iec_type_34 type_34;
            struct iec_type_35 type_35;
            struct iec_type_36 type_36;
            struct iec_type_37 type_37;
            struct iec_type_45 type_45;
            struct iec_type_46 type_46;
            struct iec_type_47 type_47;
            struct iec_type_48 type_48;
            struct iec_type_49 type_49;
            struct iec_type_50 type_50;
            struct iec_type_58 type_58;
            struct iec_type_59 type_59;
            struct iec_type_60 type_60;
            struct iec_type_61 type_61;
            struct iec_type_62 type_62;
            struct iec_type_63 type_63;
            struct iec_type_64 type_64;
            struct iec_type_100 type_100;
            struct iec_type_101 type_101;
            struct iec_type_104 type_104;
            struct iec_type_105 type_105;
            struct iec_type_107 type_107;
            struct iec_type_110 type_110;
            struct iec_type_111 type_111;
            struct iec_type_112 type_112;
            struct iec_type_113 type_113;
            struct iec_type_120 type_120;
            struct iec_type_121 type_121;
            struct iec_type_122 type_122;
            struct iec_type_123 type_123;
            struct iec_type_124 type_124;
            struct iec_type_125 type_125;
            struct iec_type_126 type_126;
        };
    };
    
#pragma pack(pop) 

#ifdef __cplusplus
}
#endif

#endif
