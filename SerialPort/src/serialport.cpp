#include "serialport.hpp"

#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <sys/termios.h>

#ifdef __MACH__
#include <AvailabilityMacros.h>
#include <mach/clock.h>
#include <mach/mach.h>
#endif

MillisecondTimer::MillisecondTimer ( const uint32_t millis ) : expiry ( timespecNow() )
{
    int64_t tvNSec = expiry.tv_nsec + ( millis * 1e6 );
    if ( tvNSec >= 1e9 ) {
        int64_t secDiff = tvNSec / static_cast<int> ( 1e9 );
        expiry.tv_nsec = tvNSec % static_cast<int> ( 1e9 );
        expiry.tv_sec += secDiff;
    } else {
        expiry.tv_nsec = tvNSec;
    }

}

int64_t MillisecondTimer::remaining()
{
    timespec now ( timespecNow() );
    int64_t millis = ( expiry.tv_sec - now.tv_sec ) * 1e3;
    millis += ( expiry.tv_nsec - now.tv_nsec ) / 1e6;
    return millis;
}

timespec MillisecondTimer::timespecNow()
{
    timespec time;
#ifdef __MACH__
    clock_serv_t cclock;
    mach_timespec_t mts;
    host_get_clock_service ( mach_host_self(), SYSTEM_CLOCK, &cclock );
    clock_get_time ( cclock, &mts );
    mach_port_deallocate ( mach_task_self(), cclock );
    time.tv_sec = mts.tv_sec;
    time.tv_nsec = mts.tv_nsec;
#else
    clock_gettime ( CLOCK_MONOTONIC, &time );
#endif
    return time;
}


timespec timespecFromeMs ( const uint32_t millis )
{
    timespec time;
    time.tv_sec = millis / 1e3;
    time.tv_nsec = ( millis - ( time.tv_sec * 1e3 ) ) * 1e6;
    return time;
}

SerialPort::SerialPort()
{

}

SerialPort::SerialPort::SerialPort ( const string& portName, BaudRate baudRate, Timeout timeout, DataBits databits,
                                     StopBits stopbits, Parity parity, FlowControl flowControl ) :
    port_name_ ( portName ), baud_rate_ ( baudRate ), data_bits_ ( databits ),
    stop_bits_ ( stopbits ), parity_ ( parity ), flow_control_ ( flowControl )
{
    pthread_mutex_init ( &this->read_mutex_, NULL );
    pthread_mutex_init ( &this->write_mutex_, NULL );

    if ( port_name_.empty() == false ) {
        open();
    }
}

SerialPort::~SerialPort()
{

}

void SerialPort::open()
{
    if ( port_name_.empty() ) {
        throw invalid_argument ( "Empty port is invalid." );
    }

    if ( is_opened_ ) {
        throw SerialPortException ( "Serial port already opened." );
    }

    fd_ = ::open ( port_name_.c_str(), O_RDWR | O_NOCTTY | O_NONBLOCK );
    if ( -1 == fd_ ) {
        switch ( errno ) {
        case EINTR:
            open();
            break;
        case ENFILE:
        case EMFILE:
            THROW ( IOException, "Too many file handles opened." );
        default:
            THROW ( IOException, errno );
        }
    }

    config();
    is_opened_ = true;
}

void SerialPort::config()
{
    if ( -1 == fd_ ) {
        THROW ( IOException, "Invalid file descriptor, please check the serial port state." );
    }

    struct termios options;

    if ( tcgetattr ( fd_, &options ) == -1 ) {
        THROW ( IOException, "::tcgetattr" );
    }

    // 这里直接关闭行模式
    options.c_cflag |= ( tcflag_t ) ( CLOCAL | CREAD );
    options.c_lflag &= ( tcflag_t ) ~ ( ICANON | ECHO | ECHOE | ECHOK | ECHONL | ISIG | IEXTEN );

    options.c_oflag &= ( tcflag_t ) ~ ( OPOST );
    options.c_iflag &= ( tcflag_t ) ~ ( INLCR | IGNCR | ICRNL | IGNBRK );
#ifdef IUCLC
    options.c_iflag &= ( tcflag_t ) ~IUCLC;
#endif
#ifdef PARMRK
    options.c_iflag &= ( tcflag_t ) ~PARMRK;
#endif

    // 波特率
    speed_t baud = B9600;
    switch ( baud_rate_ ) {
    case Baud1200:
        baud = B1200;
        break;
    case Baud2400:
        baud = B2400;
        break;
    case Baud4800:
        baud = B4800;
        break;
    case Baud9600:
        baud = B9600;
        break;
    case Baud19200:
        baud = B19200;
        break;
    case Baud38400:
        baud = B38400;
        break;
    case Baud57600:
        baud = B57600;
        break;
    case Baud115200:
        baud = B115200;
        break;
    case Baud230400:
        baud = B230400;
        break;
    default:
        baud = B9600;
        break;
    }
    ::cfsetispeed ( &options, baud );
    ::cfsetospeed ( &options, baud );

    // 数据位
    options.c_cflag &= ( tcflag_t ) ~CSIZE;
    if ( DataBits8 == data_bits_ ) {
        options.c_cflag |= CS8;
    } else if ( DataBits7 == data_bits_ ) {
        options.c_cflag |= CS7;
    } else if ( DataBits6 == data_bits_ ) {
        options.c_cflag |= CS6;
    } else if ( DataBits5 == data_bits_ ) {
        options.c_cflag |= CS5;
    } else {
        options.c_cflag |= CS8;
    }

    // 停止位
    if ( StopOne == stop_bits_ ) {
        options.c_cflag &= ( tcflag_t ) ~ ( CSTOPB );
    } else if ( StopTow == stop_bits_ ) {
        options.c_cflag |= ( CSTOPB );
    } else if ( StopOneHalf == stop_bits_ ) {
        options.c_cflag |= ( CSTOPB );
    } else {
        options.c_cflag &= ( tcflag_t ) ~ ( CSTOPB );
    }

    // 校验
    options.c_iflag &= ( tcflag_t ) ~ ( INPCK | ISTRIP );
    if ( NoParity == parity_ ) {
        options.c_cflag &= ( tcflag_t ) ~ ( PARENB | PARODD );
    } else if ( EvenParity == parity_ ) {
        options.c_cflag &= ( tcflag_t ) ~ ( PARODD );
        options.c_cflag |= ( tcflag_t ) ( PARENB );
    } else if ( OddParity == parity_ ) {
        options.c_cflag |= ( PARENB | PARODD );
    }
#ifdef CMSPAR
    else if ( MarkParity == parity_ ) {
        options.c_cflag |= ( PARENB | CMSPAR | PARODD );
    } else if ( SpaceParity == parity_ ) {
        options.c_cflag |= ( PARENB | CMSPAR );
        options.c_cflag &= ( tcflag_t ) ~ ( PARODD );
    }
#else
    else if ( MarkParity == parity_ || SpaceParity == parity_ ) {
        throw invalid_argument ( "The OS does not support mark or space parity." );
    }
#endif
    else {
        options.c_cflag &= ( tcflag_t ) ~ ( PARENB | PARODD );
    }

    // flow flow control
    if ( NoFlowControl == flow_control_ ) {
        xon_xoff_ = false;
        rts_cts_ = false;
    } else if ( SoftwareFlowControl == flow_control_ ) {
        xon_xoff_ = true;
        rts_cts_ = false;
    } else if ( HardwareFlowControl == flow_control_ ) {
        xon_xoff_ = false;
        rts_cts_ = true;
    } else {
        xon_xoff_ = false;
        rts_cts_ = false;
    }

#ifdef IXANY
    if ( xon_xoff_ ) {
        options.c_cflag |= ( IXON | IXOFF );
    } else {
        options.c_cflag &= ( tcflag_t ) ~ ( IXON | IXOFF | IXANY );
    }
#else
    if ( xon_xoff_ ) {
        options.c_cflag |= ( IXON | IXOFF );
    } else {
        options.c_cflag &= ( tcflag_t ) ~ ( IXON | IXOFF );
    }
#endif

#ifdef CRTSCTS
    if ( rts_cts_ ) {
        options.c_cflag |= ( CRTSCTS );
    } else {
        options.c_cflag &= ( unsigned long ) ~ ( CRTSCTS );
    }
#elif defined CNEW_RTSCTS
    if ( rts_cts_ ) {
        options.c_cflag |= ( CNEW_RTSCTS );
    } else {
        options.c_cflag &= ( unsigned long ) ~ ( CNEW_RTSCTS );
    }
#else
#error "The OS support seems wrong."
#endif

    options.c_cc[VMIN] = 0;
    options.c_cc[VTIME] = 0;

    uint32_t bitTimeNs = 1e9 / baud_rate_;
    byte_time_ns_ = bitTimeNs * ( 1 + data_bits_ + stop_bits_ + parity_ );

    if ( StopOneHalf == stop_bits_ ) {
        byte_time_ns_ += ( ( 1.5 - StopOneHalf ) * bitTimeNs );
    }
}

void SerialPort::close()
{
    if ( is_opened_ ) {
        if ( fd_ != -1 ) {
            int ret;
            ret = ::close ( fd_ );
            if ( 0 == ret ) {
                fd_ = -1;
            } else {
                THROW ( IOException, errno );
            }
        }

        is_opened_ = false;
    }
}

bool SerialPort::isOpened()
{
    return is_opened_;
}

size_t SerialPort::availableBytes()
{
    if ( !is_opened_ ) {
        return 0;
    }

    int count = 0;
    if ( -1 == ioctl ( fd_, FIONREAD, &count ) ) {
        THROW ( IOException, errno );
    } else {
        return static_cast<size_t> ( count );
    }
}

bool SerialPort::waitReadable ( uint32_t timeout )
{
    fd_set readFds;
    FD_ZERO ( &readFds );
    FD_SET ( fd_, &readFds );
    timespec timeoutTs ( timespecFromeMs ( timeout ) );
    int r = pselect ( fd_ + 1, &readFds, NULL, NULL, &timeoutTs, NULL );
    if ( r < 0 ) {
        if ( errno == EINTR ) {
            return false;
        }

        THROW ( IOException, errno );
    }

    if ( r == 0 ) {
        return false;
    }

    if ( !FD_ISSET ( fd_, &readFds ) ) {
        THROW ( IOException, "Select report ready to read, but our fd is not in the list, this should not happen." );
    }

    return true;
}

void SerialPort::waitByteTimes ( size_t count )
{
    timespec waitTime = {
        0,
        static_cast<long> ( byte_time_ns_ * count )
    };
    pselect ( 0, NULL, NULL, NULL, &waitTime, NULL );
}

size_t SerialPort::read ( uint8_t* buf, size_t size )
{
    if ( !is_opened_ ) {
        throw PortNotOpenedException ( "SerialPort::read" );
    }

    size_t bytesRead = 0;

    long totalTimeoutMs = timeout_.read_timeout_;
    totalTimeoutMs += timeout_.read_timeout_multiplier_ * static_cast<long> ( size );
    MillisecondTimer totalTimeout ( totalTimeoutMs );

    {
        ssize_t bytesReadNow = ::read ( fd_, buf, size );
        if ( bytesReadNow > 0 ) {
            bytesRead = bytesReadNow;
        }
    }

    while ( bytesRead < size ) {
        int64_t timeoutRemainingMs = totalTimeout.remaining();
        if ( timeoutRemainingMs <= 0 ) {
            break;
        }

        uint32_t timeout = std::min ( static_cast<uint32_t> ( timeoutRemainingMs ), timeout_.inter_byte_timeout_ );
        if ( waitReadable ( timeout ) ) {
            if ( size > 1 && timeout_.inter_byte_timeout_ == Timeout::max() ) {
                size_t bytesAvailable = availableBytes();
                if ( bytesAvailable + bytesRead < size ) {
                    waitByteTimes ( size - ( bytesAvailable + bytesRead ) );
                }
            }

            ssize_t bytesReadNow = ::read ( fd_, buf + bytesRead, size - bytesRead );

            if ( bytesReadNow < 1 ) {
                throw SerialPortException ( "Device report readiness to read, but no data returned, please ensure the device is connected." );
            }

            bytesRead += static_cast<size_t> ( bytesReadNow );
            if ( bytesRead == size ) {
                break;
            } else if ( bytesRead < size ) {
                continue;
            } else if ( bytesRead > size ) {
                throw SerialPortException ( "Read over read, too many bytes where read, this should not happen, might be a logical error." );
            }
        }
    }

    return bytesRead;
}

size_t SerialPort::write ( const uint8_t* data, size_t length )
{
    if ( !is_opened_ ) {
        throw PortNotOpenedException ( "SerialPort::write" );
    }

    fd_set writeFds;
    size_t bytesWritten = 0;

    long totalTimeoutMs = timeout_.write_timeout_;
    totalTimeoutMs += timeout_.write_timeout_multiplier_ * static_cast<long> ( length );
    MillisecondTimer totalTimeout ( totalTimeoutMs );

    bool firstIteration = true;
    while ( bytesWritten < length ) {
        int64_t timeoutRemainingMs = totalTimeout.remaining();
        if ( !firstIteration && ( timeoutRemainingMs <= 0 ) ) {
            break;
        }
        firstIteration = false;

        timespec timeout ( timespecFromeMs ( timeoutRemainingMs ) );
        FD_ZERO ( &writeFds );
        FD_SET ( fd_, &writeFds );

        int r = pselect ( fd_ + 1, NULL, &writeFds, NULL, &timeout, NULL );
        if ( r < 0 ) {
            if ( errno == EINTR ) {
                continue;
            }

            THROW ( IOException, "SerialPort::write" );
        }

        if ( r == 0 ) {
            break;
        }

        if ( r > 0 ) {
            ssize_t bytesWrittenNow = ::write ( fd_, data + bytesWritten, length - bytesWritten );
            if ( bytesWrittenNow < 1 ) {
                throw SerialPortException ( "Device report readiness to write, but no date returned, please ensure the device is connected." );
            }

            bytesWritten += static_cast<size_t> ( bytesWrittenNow );
            if ( bytesWritten == length ) {
                break;
            } else if ( bytesWritten < length ) {
                continue;
            } else if ( bytesWritten > length ) {
                throw SerialPortException ( "Wite over wrote, too many bytes where written, this should not happen, might be a logical error!" );
            }
        }

        THROW ( IOException, "Select reports ready to write, but our fd is not in the list, this should not happen!" );
    }

    return bytesWritten;
}

void SerialPort::setPort ( const string& port )
{
    port_name_ = port;
}


std::string SerialPort::getPort() const
{
    return port_name_;
}

void SerialPort::setTimeout ( Timeout& timeout )
{
    this->timeout_ = timeout;
}

Timeout SerialPort::getTimeout() const
{
    return this->timeout_;
}

void SerialPort::setBaudrate ( BaudRate baudRate )
{
    this->baud_rate_ = baudRate;
}

BaudRate SerialPort::getBaudrate() const
{
    return this->baud_rate_;
}

void SerialPort::setDataBits ( DataBits dataBits )
{
    this->data_bits_ = dataBits;
}

DataBits SerialPort::getDataBits() const
{
    return this->data_bits_;
}

void SerialPort::setStopBits ( StopBits stopBits )
{
    this->stop_bits_ = stopBits;
}

StopBits SerialPort::getStopBits() const
{
    return this->stop_bits_;
}

void SerialPort::setParity ( Parity parity )
{
    this->parity_ = parity;
}

Parity SerialPort::getParity() const
{
    return this->parity_;
}

void SerialPort::setFlowControl ( FlowControl flowControl )
{
    this->flow_control_ = flowControl;
}

FlowControl SerialPort::getFlowControl() const
{
    return flow_control_;
}

void SerialPort::flush()
{
    if ( !is_opened_ ) {
        throw PortNotOpenedException ( "SerialPort::flush" );
    }
    tcdrain ( fd_ );
}

void SerialPort::flushInput()
{
    if ( !is_opened_ ) {
        throw PortNotOpenedException ( "SerialPort::flushInput" );
    }

    tcflush ( fd_, TCIFLUSH );
}

void SerialPort::flushOutput()
{
    if ( !is_opened_ ) {
        throw PortNotOpenedException ( "SerialPort::flushOutput" );
    }

    tcflush ( fd_, TCOFLUSH );
}

void SerialPort::sendBreak ( int duration )
{
    if ( !is_opened_ ) {
        throw PortNotOpenedException ( "SerialPort::sendBreak" );
    }

    tcsendbreak ( fd_, static_cast<int> ( duration ) );
}

void SerialPort::setBreak ( bool level )
{
    if ( !is_opened_ ) {
        throw PortNotOpenedException ( "SerialPort::setBreak" );
    }

    if ( level ) {
        if ( -1 == ioctl ( fd_, TIOCSBRK ) ) {
            stringstream ss;
            ss << "setBreak failed on a call to ioctl(TIOCSBRK): " << errno << " " << strerror ( errno );
            throw ( SerialPortException ( ss.str().c_str() ) );
        }
    } else {
        if ( -1 == ioctl ( fd_, TIOCCBRK ) ) {
            stringstream ss;
            ss << "setBreak failed on a call to ioctl(TIOCCBRK): " << errno << " " << strerror ( errno );
            throw ( SerialPortException ( ss.str().c_str() ) );
        }
    }
}

void SerialPort::setRTS ( bool level )
{
    if ( !is_opened_ ) {
        throw PortNotOpenedException ( "SerialPort::setRTS" );
    }

    int command = TIOCM_RTS;

    if ( level ) {
        if ( -1 == ioctl ( fd_, TIOCMBIS, &command ) ) {
            stringstream ss;
            ss << "setRTS failed on a call to ioctl(TIOCMBIS): " << errno << " " << strerror ( errno );
            throw ( SerialPortException ( ss.str().c_str() ) );
        }
    } else {
        if ( -1 == ioctl ( fd_, TIOCMBIC, &command ) ) {
            stringstream ss;
            ss << "setRTS failed on a call to ioctl(TIOCMBIC): " << errno << " " << strerror ( errno );
            throw ( SerialPortException ( ss.str().c_str() ) );
        }
    }
}

void SerialPort::setDTR ( bool level )
{
    if ( !is_opened_ ) {
        throw PortNotOpenedException ( "SerialPort::setDTR" );
    }

    int command = TIOCM_DTR;

    if ( level ) {
        if ( -1 == ioctl ( fd_, TIOCMBIS, &command ) ) {
            stringstream ss;
            ss << "setDTR failed on a call to ioctl(TIOCMBIS): " << errno << " " << strerror ( errno );
            throw ( SerialPortException ( ss.str().c_str() ) );
        }
    } else {
        if ( -1 == ioctl ( fd_, TIOCMBIC, &command ) ) {
            stringstream ss;
            ss << "setDTR failed on a call to ioctl(TIOCMBIC): " << errno << " " << strerror ( errno );
            throw ( SerialPortException ( ss.str().c_str() ) );
        }
    }
}

bool SerialPort::waitForChange()
{
#ifndef TIOCMIWAIT
    while ( is_opened_ ) {
        int status;

        if ( -1 == ioctl ( fd_, TIOCMGET, &status ) ) {
            stringstream ss;
            ss << "waitForChange failed on a call to ioctl(TIOCMGET): " << errno << " " << strerror ( errno );
            throw ( SerialPortException ( ss.str().c_str() ) );
        } else {
            if ( 0 != ( status & TIOCM_CTS )
                    || 0 != ( status & TIOCM_DSR )
                    || 0 != ( status & TIOCM_RI )
                    || 0 != ( status & TIOCM_CD ) ) {
                return true;
            }
        }

        usleep ( 1000 );
    }
    return false;
#else
    int command = ( TIOCM_CD | TIOCM_DSR | TIOCM_RI | TIOCM_CTS );

    if ( -1 == ioctl ( fd_, TIOCMIWAIT, &command ) ) {
        stringstream ss;
        ss << "waitForDSR failed on a call to ioctl(TIOCMIWAIT): "
           << errno << " " << strerror ( errno );
        throw ( SerialPortException ( ss.str().c_str() ) );
    }
    return true;
#endif
}

bool SerialPort::getCTS()
{
    if (!is_opened_) {
        throw PortNotOpenedException ( "SerialPort::getCTS" );
    }
    
    int status;
    
    if (-1 == ioctl(fd_, TIOCMGET, &status)) {
        stringstream ss;
        ss << "getCTS failed on a call to ioctl(TIOCMGET): " << errno << " " << strerror(errno);
        throw(SerialPortException(ss.str().c_str()));
    } else {
        return 0 != (status & TIOCM_CTS);
    }
}

bool SerialPort::getDSR()
{
    if (!is_opened_) {
        throw PortNotOpenedException ( "SerialPort::getDSR" );
    }
    
    int status;
    
    if (-1 == ioctl(fd_, TIOCMGET, &status)) {
        stringstream ss;
        ss << "getDSR failed on a call to ioctl(TIOCMGET): " << errno << " " << strerror(errno);
        throw(SerialPortException(ss.str().c_str()));
    } else {
        return 0 != (status & TIOCM_DSR);
    }
}

bool SerialPort::getRI()
{
    if (!is_opened_) {
        throw PortNotOpenedException ( "SerialPort::getRI" );
    }
    
    int status;
    
    if (-1 == ioctl(fd_, TIOCMGET, &status)) {
        stringstream ss;
        ss << "getRI failed on a call to ioctl(TIOCMGET): " << errno << " " << strerror(errno);
        throw(SerialPortException(ss.str().c_str()));
    } else {
        return 0 != (status & TIOCM_RI);
    }
}

bool SerialPort::getCD()
{
    if (!is_opened_) {
        throw PortNotOpenedException ( "SerialPort::getCD" );
    }
    
    int status;
    
    if (-1 == ioctl(fd_, TIOCMGET, &status)) {
        stringstream ss;
        ss << "getCD failed on a call to ioctl(TIOCMGET): " << errno << " " << strerror(errno);
        throw(SerialPortException(ss.str().c_str()));
    } else {
        return 0 != (status & TIOCM_CD);
    }
}

void SerialPort::readLock()
{
    int result = pthread_mutex_lock(&this->read_mutex_);
    if (result) {
        THROW(IOException, result);
    }
}

void SerialPort::readUnlock()
{
    int result = pthread_mutex_unlock(&this->read_mutex_);
    if (result) {
        THROW(IOException, result);
    }
}

void SerialPort::writeLock()
{
    int result = pthread_mutex_lock(&this->write_mutex_);
    if (result) {
        THROW(IOException, result);
    }
}

void SerialPort::writeUnlock()
{
    int result = pthread_mutex_unlock(&this->write_mutex_);
    if (result) {
        THROW(IOException, result);
    }
}
