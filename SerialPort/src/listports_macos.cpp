#if defined(__APPLE__)

#include <CoreFoundation/CoreFoundation.h>
#include <IOKit/IOBSD.h>
#include <IOKit/IOKitLib.h>
#include <IOKit/serial/IOSerialKeys.h>
#include <stdint.h>
#include <sys/param.h>

#include <iostream>
#include <string>
#include <vector>

#include "serialport.hpp"

#define HARDWARE_ID_STRING_LENGTH 128

string cfstringToString(CFStringRef cfstring);
string getDevicePath(io_object_t &serialPort);
string getClassName(io_object_t &obj);
io_registry_entry_t getParentIOUsbDevice(io_object_t &serialPort);
string getStringProperty(io_object_t &device, const char *property);
uint16_t getIntProperty(io_object_t &device, const char *property);
string trrim(const string &str);

string cfstringToString(CFStringRef cfstring) {
    char cstring[MAXPATHLEN];
    string result;

    if (cfstring) {
        Boolean success = CFStringGetCString(cfstring, cstring, sizeof(cstring), kCFStringEncodingASCII);
        if (success) {
            result = cstring;
        }
    }
    return result;
}

string getDevicePath(io_object_t &serialPort) {
    CFTypeRef callOutPath;
    string devPath;

    callOutPath = IORegistryEntryCreateCFProperty(serialPort, CFSTR(kIOCalloutDeviceKey), kCFAllocatorDefault, 0);
    if (callOutPath) {
        if (CFGetTypeID(callOutPath) == CFStringGetTypeID()) {
            devPath = cfstringToString(static_cast<CFStringRef>(callOutPath));
        }

        CFRelease(callOutPath);
    }

    return devPath;
}

string getClassName(io_object_t &obj) {
    string result;
    io_name_t className;
    kern_return_t kernResult;

    kernResult = IOObjectGetClass(obj, className);
    if (kernResult == KERN_SUCCESS) {
        result = className;
    }

    return result;
}

io_registry_entry_t getParentIOUsbDevice(io_object_t &serialPort) {
    io_object_t dev = serialPort;
    io_registry_entry_t parent = 0;
    io_registry_entry_t result = 0;
    kern_return_t kernResult = KERN_FAILURE;
    string name = getClassName(dev);

    while (name != "IOUSBDevice") {
        kernResult = IORegistryEntryGetParentEntry(dev, kIOServicePlane, &parent);

        if (kernResult != KERN_SUCCESS) {
            result = 0;
            break;
        }

        dev = parent;

        name = getClassName(dev);
    }

    if (kernResult == KERN_SUCCESS) {
        result = dev;
    }

    return result;
}

string getStringProperty(io_object_t &dev, const char *property) {
    string propertyName;

    if (dev) {
        CFStringRef propertyAsCFString =
            CFStringCreateWithCString(kCFAllocatorDefault, property, kCFStringEncodingASCII);
        CFTypeRef nameAsCFString = IORegistryEntryCreateCFProperty(dev, propertyAsCFString, kCFAllocatorDefault, 0);

        if (nameAsCFString) {
            if (CFGetTypeID(nameAsCFString) == CFStringGetTypeID()) {
                propertyName = cfstringToString(static_cast<CFStringRef>(nameAsCFString));
            }
            CFRelease(nameAsCFString);
        }

        if (propertyAsCFString) {
            CFRelease(propertyAsCFString);
        }
    }
    return propertyName;
}

uint16_t getIntProperty(io_object_t &dev, const char *property) {
    uint16_t result = 0;

    if (dev) {
        CFStringRef propertyAsCFString =
            CFStringCreateWithCString(kCFAllocatorDefault, property, kCFStringEncodingASCII);
        CFTypeRef number = IORegistryEntryCreateCFProperty(dev, propertyAsCFString, kCFAllocatorDefault, 0);

        if (propertyAsCFString) {
            CFRelease(propertyAsCFString);
        }

        if (number) {
            if (CFGetTypeID(number) == CFNumberGetTypeID()) {
                bool success = CFNumberGetValue(static_cast<CFNumberRef>(number), kCFNumberSInt16Type, &result);
                if (!success) {
                    result = 0;
                }
            }
            CFRelease(number);
        }
    }

    return result;
}

string rtrim(const string &str) {
    string result = str;
    string whitespace = " \t\f\v\v\r";

    std::size_t found = result.find_last_not_of(whitespace);

    if (found != std::string::npos) {
        result.erase(found + 1);
    } else {
        result.clear();
    }

    return result;
}

vector<PortInfo> listPorts() {
    vector<PortInfo> devicesFound;
    CFMutableDictionaryRef classesToMatch;
    io_iterator_t serialPortIterator;
    io_object_t serialPort;
    mach_port_t masterPort;
    kern_return_t kernResult;

    kernResult = IOMasterPort(MACH_PORT_NULL, &masterPort);
    if (kernResult != KERN_SUCCESS) {
        return devicesFound;
    }

    classesToMatch = IOServiceMatching(kIOSerialBSDServiceValue);
    if (classesToMatch == NULL) {
        return devicesFound;
    }

    CFDictionarySetValue(classesToMatch, CFSTR(kIOSerialBSDTypeKey), CFSTR(kIOSerialBSDAllTypes));
    kernResult = IOServiceGetMatchingServices(masterPort, classesToMatch, &serialPortIterator);
    if (kernResult != KERN_SUCCESS) {
        return devicesFound;
    }

    while ((serialPort = IOIteratorNext(serialPortIterator))) {
        string devPath = getDevicePath(serialPort);
        io_registry_entry_t parent = getParentIOUsbDevice(serialPort);
        IOObjectRelease(serialPort);

        if (devPath.empty()) {
            continue;
        }

        PortInfo portInfo;
        portInfo.port_ = devPath;
        portInfo.description_ = "n/a";
        portInfo.hardware_id_ = "n/a";

        string devName = rtrim(getStringProperty(parent, "USB Product Name"));
        string vendorName = rtrim(getStringProperty(parent, "USB Vendor Name"));
        string description = rtrim(vendorName + " " + devName);
        if (!description.empty()) {
            portInfo.description_ = description;
        }

        string serialNumber = rtrim(getStringProperty(parent, "USB Serial Number"));
        uint16_t vendorId = getIntProperty(parent, "idVendor");
        uint16_t productId = getIntProperty(parent, "idProduct");

        if (vendorId && productId) {
            char cstring[HARDWARE_ID_STRING_LENGTH];

            if (serialNumber.empty()) {
                serialNumber = "None";
            }

            int ret = snprintf(cstring, HARDWARE_ID_STRING_LENGTH, "USB VID:PID=%04x:%04x SNR=%s", vendorId, productId,
                               serialNumber.c_str());
            if ((ret >= 0) && (ret < HARDWARE_ID_STRING_LENGTH)) {
                portInfo.hardware_id_ = cstring;
            }
        }

        devicesFound.push_back(portInfo);
    }

    IOObjectRelease(serialPortIterator);
    return devicesFound;
}

#endif
