#ifndef SERIALPORT_H
#define SERIALPORT_H

#include <limits>
#include <vector>
#include <string>
#include <sstream>
#include <exception>
#include <stdexcept>

#include <pthread.h>

#include <string.h>

#define THROW(exceptionClass, message) throw exceptionClass(__FILE__, __LINE__, (message) )

using namespace std;


class MillisecondTimer {
public:
    MillisecondTimer(const uint32_t millis);
    int64_t remaining();
    
private:
    static timespec timespecNow();
    timespec expiry;
};

typedef enum BaudRate {
    Baud1200 = 1200,
    Baud2400 = 2400,
    Baud4800 = 4800,
    Baud9600 = 9600,
    Baud19200 = 19200,
    Baud38400 = 38400,
    Baud57600 = 57600,
    Baud115200 = 115200,
    Baud230400 = 230400,
    UnknownBaud = -1
} BaudRate;

typedef enum DataBits {
    DataBits5 = 5,
    DataBits6 = 6,
    DataBits7 = 7,
    DataBits8 = 8,
    UnknownDataBits = -1,
} DataBits;

typedef enum Parity {
    NoParity = 0,
    EvenParity = 1,
    OddParity = 2,
    SpaceParity = 3,
    MarkParity = 4,
    UnknowParity = -1,
}  Parity;

typedef enum StopBits {
    StopOne = 1,
    StopTow = 2,
    StopOneHalf = 3,
    UnknowStopBits = -1,
} StopBits;

typedef enum FlowControl {
    NoFlowControl,
    HardwareFlowControl,
    SoftwareFlowControl,
    UnknowFlowControl = -1,
} FlowControl;

struct Timeout {
    uint32_t inter_byte_timeout_;
    uint32_t read_timeout_;
    uint32_t read_timeout_multiplier_;
    uint32_t write_timeout_;
    uint32_t write_timeout_multiplier_;
    
private:
    static const uint32_t TIMEOUT_MAX = 5 * 60 * 1000;

public:
    static uint32_t max()
    {
        return TIMEOUT_MAX;
    }

    explicit Timeout ( uint32_t interByteTimeout = 0,
                       uint32_t readTimeout = 0,
                       uint32_t readTimeoutMultiplier = 0,
                       uint32_t writeTimeout = 0,
                       uint32_t writeTimeoutMultiplier = 0 )
        : inter_byte_timeout_ ( interByteTimeout ),
          read_timeout_ ( readTimeout ), read_timeout_multiplier_ ( readTimeoutMultiplier ),
          write_timeout_ ( writeTimeout ), write_timeout_multiplier_ ( writeTimeoutMultiplier )
    {

    }
};

class SerialPort
{
public:
    explicit SerialPort();
    explicit SerialPort ( const string &portName = "", BaudRate baudRate = Baud9600, Timeout timeout = Timeout(),
                          DataBits databits = DataBits8, StopBits stopbits = StopOne, Parity parity = NoParity,
                          FlowControl flowControl = NoFlowControl );
    virtual ~SerialPort();

    void open();
    bool isOpened();

    void close();
    
    size_t availableBytes();
    bool waitReadable(uint32_t timeout);
    void waitByteTimes(size_t count);
    
    size_t read(uint8_t *buf, size_t size = 1);
    size_t write(const uint8_t *data, size_t = 1);
    
    void flush();
    
    void flushInput();
    void flushOutput();
    
    void sendBreak(int duration);
    void setBreak(bool level);
    void setRTS(bool level);
    void setDTR(bool level);
    bool waitForChange();
    
    bool getCTS();
    bool getDSR();
    bool getRI();
    bool getCD();
    
    void setPort(const string &port);
    string getPort() const;
    
    void setTimeout(Timeout &timeout);
    Timeout getTimeout() const;
    
    void setBaudrate(BaudRate baudRate);
    BaudRate getBaudrate() const;
    
    void setDataBits(DataBits dataBits);
    DataBits getDataBits() const;
    
    void setStopBits(StopBits stopBits);
    StopBits getStopBits() const;
    
    void setParity(Parity parity);
    Parity getParity() const;
    
    void setFlowControl(FlowControl flowControl);
    FlowControl getFlowControl() const;
    
    void readLock();
    void readUnlock();
    void writeLock();
    void writeUnlock();

protected:
    void config();

private:
    int fd_;
    
    std::string port_name_;
    BaudRate baud_rate_;
    Timeout timeout_;
    DataBits data_bits_;
    StopBits stop_bits_;
    Parity parity_;
    FlowControl flow_control_;
    
    uint32_t byte_time_ns_;
    
    bool is_opened_ = false;
    bool xon_xoff_ = false;
    bool rts_cts_ = false;
    
    pthread_mutex_t read_mutex_;
    pthread_mutex_t write_mutex_;
};

class SerialPortException : public std::exception {
    SerialPortException& operator=(const SerialPortException&);
    string ex_what_;
    
public:
    SerialPortException(const char *desc) {
        stringstream ss;
        ss << "SerialPortException " << desc << " failed.";
        ex_what_ = ss.str();
    }
    
    SerialPortException(const SerialPortException &other) : ex_what_(other.ex_what_) {}
    
    virtual ~SerialPortException() throw() {}
    virtual const char *what() const throw() {
        return ex_what_.c_str();
    }
};

class IOException : public std::exception {
    IOException& operator=(const IOException&);
    string file_;
    int line_;
    string ex_what_;
    int errno_;
    
public:
    explicit IOException(string file, int line, int errnum)
        : file_(file), line_(line), errno_(errnum) {
        stringstream ss;
        char *errorStr = strerror(errno_);
        ss << "IOException (" << errno_ << "): " << errorStr;
        ss << ", file " << file_ << ", line " << line_ << ".";
        ex_what_ = ss.str();
    }
    
    explicit IOException(string file, int line, const char *desc)
    : file_(file), line_(line), errno_(0){
        stringstream ss;
        ss << "IOException: " << "desc" << ", file " << file_ << ", line " << line_ << ".";
        ex_what_ = ss.str();
    }
    
    IOException(const IOException &other) : line_(other.line_), ex_what_(other.ex_what_), errno_(other.errno_) {
        
    }
    
    virtual ~IOException() throw() {
        
    }
    
    int getErrorNumber() const {
        return errno_;
    }
    
    virtual const char * what() const throw() {
        return ex_what_.c_str();
    }
};

class PortNotOpenedException : public std::exception {
    const PortNotOpenedException& operator=(PortNotOpenedException);
    std::string ex_what_;
    
public:
    PortNotOpenedException(const char *desc) {
        std::stringstream ss;
        ss << "PortNotOpenedException " << desc << " failed.";
        ex_what_ = ss.str();
    }
    
    PortNotOpenedException(const PortNotOpenedException &other) : ex_what_(other.ex_what_) {}
    
    virtual ~PortNotOpenedException() throw() {}
    virtual const char* what() const throw() {
        return ex_what_.c_str();
    }
};

struct PortInfo {
    std::string port_;
    std::string description_;
    std::string hardware_id_;
};

std::vector<PortInfo> listPorts();

#endif // SERIALPORT_H
