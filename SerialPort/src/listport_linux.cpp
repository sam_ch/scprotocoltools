#if defined(__linux__)

#include "serialport.hpp"

#include <vector>
#include <string>
#include <sstream>
#include <stdexcept>
#include <iostream>
#include <fstream>
#include <cstdio>
#include <cstdarg>
#include <cstdlib>

#include <glob.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

static vector<string> glob(const vector<string> &patterns);
static string baseName(const string &path);
static string dirName(const string &path);
static bool pathExists(const string &path);
static string realPath(const string &path);
static string usbSysfsFriendlyName(const string &sysUsbPath);
static vector<string> getSysfsInfo(const string &devicePath);
static string readLine(const string &file);
static string usbSysfsHWString(const string &sysfsPath);
static string format(const char *format, ...);

vector<std::string> glob ( const vector<std::string>& patterns ) {
    vector<string> pathsFound;
    if (patterns.size() == 0) {
        return pathsFound;
    }
    
    glob_t globResults;
    int globRetval = glob(patterns[0].c_str(), 0, NULL, &globResults);
    
    vector<string>::const_iterator iter = patterns.begin();
    while(++iter != patterns.end()) {
        globRetval = glob(iter->c_str(), GLOB_APPEND, NULL, &globResults);
    }
    
    for (int pathIndex = 0; pathIndex < globResults.gl_pathc; pathIndex++) {
        pathsFound.push_back(globResults.gl_pathv[pathIndex]);
    }
    
    globfree(&globResults);
    
    return pathsFound;
}

std::string baseName ( const std::string& path ) {
    size_t pos = path.rfind("/");
    if (pos == std::string::npos) {
        return path;
    }
    
    return string(path, pos + 1, string::npos);
}

std::string dirName ( const std::string& path ) {
    size_t pos = path.rfind("/");
    if (pos == std::string::npos) {
        return path;
    } else if (pos == 0) {
        return "/";
    }
    
    return string(path, 0, pos);
}

bool pathExists ( const std::string& path ) {
    struct stat sb;
    
    if (stat(path.c_str(), &sb) == 0) {
        return true;
    }
    
    return false;
}

std::string realPath ( const std::string& path ) {
    char *real_path = realpath(path.c_str(), NULL);
    string result;
    if (real_path != NULL) {
        result = real_path;
        free(real_path);
    }
    
    return result;
}

std::string usbSysfsFriendlyName ( const std::string& sysUsbPath ) {
    unsigned int deviceNumber = 0;
    istringstream(readLine(sysUsbPath + "/devnum")) >> deviceNumber;
    string manufacturer = readLine(sysUsbPath + "/manufacturer");
    string product = readLine(sysUsbPath + "/product");
    string serial = readLine(sysUsbPath + "/serial");
    if (manufacturer.empty() && product.empty() && serial.empty()) {
        return "";
    }
    
    return format("%s %s %s", manufacturer.c_str(), product.c_str(), serial.c_str());
}

vector<std::string> getSysfsInfo ( const std::string& devicePath ) {
    string devName = baseName(devicePath);
    string friendlyName;
    string hwId;
    string sysDevPath = format("/sys/class/tty/%s/device", devName.c_str());
    
    if (devName.compare(0, 6, "ttyUSB") == 0) {
        sysDevPath = dirName(dirName(readLine(sysDevPath)));
        if (pathExists(sysDevPath)) {
            friendlyName = usbSysfsFriendlyName(sysDevPath);
            hwId = usbSysfsHWString(sysDevPath);
        }
    } else if (devName.compare(0, 6, "ttyACM") == 0) {
        sysDevPath = dirName(readLine(sysDevPath));
        if (pathExists(sysDevPath)) {
            friendlyName = usbSysfsFriendlyName(sysDevPath);
            hwId = usbSysfsHWString(sysDevPath);
        }
    } else {
        string sysIdPath = sysDevPath + "/id";
        if (pathExists(sysIdPath)) {
            hwId = readLine(sysIdPath);
        }
    }
    
    if (friendlyName.empty()) {
        friendlyName = devName;
    }
    
    if (hwId.empty()) {
        hwId = "n/a";
    }
    
    vector<string> result;
    result.push_back(friendlyName);
    result.push_back(hwId);
    
    return result;
}



std::string usbSysfsHWString ( const std::string& sysfsPath ) {
    string serialNumber = readLine(sysfsPath + "/serial");
    
    if (serialNumber.length() > 0) {
        serialNumber = format("SNR=%s", serialNumber.c_str());
    }
    
    string vid = readLine(sysfsPath + "/idVender");
    string pid = readLine(sysfsPath + "/idProduct");
    
    return format("USB VID:PID=%s:%s %s", vid.c_str(), pid.c_str(), serialNumber.c_str());
}

std::string readLine ( const std::string& file ) {
    ifstream ifs(file.c_str(), ifstream::in);
    string line;
    if (ifs) {
        getline(ifs, line);
    }
    
    return line;
}

std::string format ( const char* format, ... ) {
    va_list ap;
    size_t bufSizeBytes = 256;
    string result;
    char *buf = (char*)malloc(bufSizeBytes);
    if (buf == NULL) {
        return result;
    }
    
    bool done = false;
    unsigned int loopCount = false;
    while (!done) {
        va_start(ap, format);
        int returnVal = vsnprintf(buf, bufSizeBytes, format, ap);
        if (returnVal < 0) {
            done = true;
        } else if (returnVal >= bufSizeBytes) {
            bufSizeBytes = returnVal + 1;
            char *newBufPtr = (char*)realloc(buf, bufSizeBytes);
            if (newBufPtr == NULL) {
                done = true;
            } else {
                buf = newBufPtr;
            }
        } else {
            result = buf;
            done = true;
        }
        
        va_end(ap);
        
        if (++loopCount > 5) {
            done = true;
        }
    }
    
    free(buf);
    return result;
}

vector<PortInfo> listPorts() {
    vector<PortInfo> results;
    
    vector<string> searchGlobs;
    searchGlobs.push_back("/dev/ttyACM*");
    searchGlobs.push_back("/dev/ttyS*");
    searchGlobs.push_back("/dev/ttyUSB*");
    searchGlobs.push_back("/dev/tty.*");
    searchGlobs.push_back("/dev/cu.*");
    
    vector<string> devFound = glob(searchGlobs);
    vector<string>::iterator iter = devFound.begin();
    
    while(iter != devFound.end()) {
        string dev = *iter++;
        vector<string> sysfsInfo = getSysfsInfo(dev);
        string friendlyName = sysfsInfo[0];
        string hwId = sysfsInfo[1];
        
        PortInfo devEntity;
        devEntity.port_ = dev;
        devEntity.description_ = friendlyName;
        devEntity.hardware_id_ = hwId;
        
        results.push_back(devEntity);
    }
    
    return results;
}

#endif
