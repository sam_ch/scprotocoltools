cmake_minimum_required(VERSION 3.0)
project(SerialPort)

# Find includes in corresponding build directories
set(CMAKE_INCLUDE_CURRENT_DIR ON)

set(serialport_SRC
  src/serialport.cpp
  src/listports_macos.cpp
  src/listport_linux.cpp
)

# Tell CMake to create the helloworld executable
# add_executable(serialport ${serialport_SRC})
add_library(serialport ${serialport_SRC})

set(THREADS_PREFER_PTHREAD_FLAG ON)
find_package(Threads REQUIRED)
target_link_libraries(serialport Threads::Threads)

# Install the executable
install(TARGETS serialport DESTINATION bin)
